# `Markdoc` on the [FluxFlex](http://www.fluxflex.com/)

## What it `Markdoc`

QUOTE: [Markdoc](http://markdoc.org/)

> Markdoc is a lightweight Markdown-based wiki system.
> It's been designed to allow you create and manage wikis quickly and easily as possible.

`Markdoc` is created by Mr. [Zachary Voase](http://zacharyvoase.com/)

## What is this package

This package is `Markdoc` toolkit for [fluxflex.com](http://fluxflex.com/).

## How to use

    $ git clone git://github.com/nyarla/fluxflex-markdoc.git my-wiki # setup
    $ cd my-wiki
    $ emacs wiki/index.md # Edit your page
    $ git remote add fluxflex {YOUR-GIT-REPOSITORY-ON-FLUXFLEX} # adding remote repository on fluxflex.com
    $ git push fluxflex master # auto building and deploying your wiki on fluxflex.com

## Author of this package

Naoki Okamura (Nyarla) *nyarla[ at ]thotep.net* (I'm Japanese)

## License

This package is under public domain.
